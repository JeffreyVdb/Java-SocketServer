/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package chatserv;
import java.util.HashMap;
/**
 * This class checks commands to see if they're valid and also maps them to their function
 * This class could also be used to work with plugins
 * @author Nick
 */
public class CommandParser {

    // Default commands
    private HashMap<String, String> defaults;

    public CommandParser()
    {
        defaults.put("/help", "Shows the help");
        defaults.put("/bcast", "Broadcast something");
    }


}
