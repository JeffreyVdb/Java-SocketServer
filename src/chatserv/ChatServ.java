/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserv;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author jeffrey
 */
public class ChatServ {
    private Map<InetAddress, String> users;
    private ServerSocket servSock;
    private int port = 7777;
    private boolean started = false;
    private ArrayList<Socket> sockets;

    public ChatServ() {
    }

    public ChatServ(int port) {
        this.port = port;
    }

    public void handleConnections() {
        if (started) {
            return;
        }

        sockets = new ArrayList<Socket>();
        started = true;
        users = Collections.synchronizedMap(new HashMap<InetAddress, String>());
        try {

            this.servSock = new ServerSocket(7777);           
            System.out.println("Connected");
            System.out.println(this.servSock.getInetAddress());
            System.out.println();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            started = false;
            return;
        }

        while (true) {
            // Keep accepting connections
            try {
                Socket incoming = servSock.accept();           
                if (users.get(incoming.getInetAddress()) != null)
                {
                    continue;
                }
                
                System.out.println("Accepting: " + incoming.getInetAddress());                
                ExecutorService service = Executors.newCachedThreadPool();
                service.execute(new ConnectHandler(incoming, users, sockets));
                service.shutdown();
            } catch (Exception e) {
                System.err.println("Error while waiting for connections: " + e.getMessage());
            }
        }
    }
}
