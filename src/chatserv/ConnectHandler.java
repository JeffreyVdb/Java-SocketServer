/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author jeffrey
 */
public class ConnectHandler implements Runnable {

    private Socket sock;
    private PrintWriter writer;
    private BufferedReader reader;
    private Map<InetAddress, String> users;
    private ArrayList<Socket> sockets;

    public ConnectHandler(Socket sock, Map<InetAddress, String> users, ArrayList<Socket> sockets) {
        this.sock = sock;
        this.users = users;
        this.users.put(sock.getInetAddress(), "Anon");
        this.sockets = sockets;
        this.sockets.add(sock);

        reportOnlineUsers(sock.getInetAddress());
    }

    private void printOnlineUserList() {
        writer.write("--Online users--\n");

        for (InetAddress a : users.keySet()) {
            writer.write(String.format("User: [IP] %s - [NAME] %s\n", a.getHostAddress(), users.get(a)));
        }

        writer.flush();
    }

    private void checkCommand(String command) {
        System.out.println("Received from " + sock.getInetAddress().getHostAddress() + ": " + command + "\n");

        if (!command.startsWith("/")) {
            writer.write("No command used. Using /bcast.");
            writer.flush();
            checkCommand("/bcast " + command.trim());
            return;
        }

        String data[];
        data = command.trim().split(" ");
        String firstWord = data[0];

        firstWord = firstWord.substring(1, firstWord.length());
        if (firstWord.equals("online")) {
            printOnlineUserList();
        } else if (firstWord.equals("name")) {
            if (data.length != 2) {
                writer.write("Names cannot contain spaces.");
                writer.flush();
                return;
            }

            String oldName = users.get(sock.getInetAddress());
            users.put(sock.getInetAddress(), data[1]);
            try {
                for (Socket s : sockets) {
                    PrintWriter w = new PrintWriter(s.getOutputStream(), true);
                    w.write(String.format("%s is now known as %s\n", oldName, data[1]));
                    w.flush();
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        } else if (firstWord.equals("bcast")) {
            try {
                System.out.println("--- Broadcasting to all users.");
                String name = users.get(sock.getInetAddress());

                for (Socket s : sockets) {
                    PrintWriter w = new PrintWriter(s.getOutputStream(), true);
                    w.write(String.format("[BROADCAST] %s: %s\n", name, command.substring(command.indexOf(' ') + 1, command.length())));
                    w.flush();
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        } else if(firstWord.equals("/userlist")){
          try{
              PrintWriter w = new PrintWriter(sock.getOutputStream(), true);
              w.write(this.apiGetUsers());
              w.flush();
          } catch(Exception e){
              System.err.println(e.getMessage());
          }
        } else {
            writer.write("Command not found.");
            writer.flush();
        }
    }

    @Override
    public void run() {
        try {
            writer = new PrintWriter(sock.getOutputStream(), true);
            writer.write("Welcome " + sock.getInetAddress() + "\n");
            writer.flush();

            printOnlineUserList();

            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return;
        }

        while (sock.isConnected() && !sock.isInputShutdown()) {
            // Read message that is sent to server
            try {
                int bytesReceived = 0;
                CharBuffer buffer = CharBuffer.allocate(512);
                if ((bytesReceived = reader.read(buffer)) < 1) {
                    System.out.println("Closing: " + sock.getInetAddress());
                    break;
                }

                buffer.flip();
                // Evaluate command
                checkCommand(buffer.toString());
            } catch (IOException e) {
                System.err.println(e.getMessage());
                break;
            } catch (NullPointerException nullExcept) {
                break;
            }
        }

        // Close everything
        InetAddress toRemove = sock.getInetAddress();
        try {
            writer.close();
            reader.close();
            sock.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            System.out.println("Removing " + sock.getInetAddress().getHostAddress());
            users.remove(toRemove);

        }
    }

    private void reportOnlineUsers(InetAddress addr) {

        for (Socket s : sockets) {
            if (s.getInetAddress() == addr) {
                continue;
            }

            try {
                PrintWriter w = new PrintWriter(s.getOutputStream(), true);
                w.write(String.format("%s has just connected\n", addr.getHostAddress()));
                w.write(String.format("Online users: %d\n", users.size()));
                w.flush();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }

    }

    /**
     * Get the users in a 3rd party consumable format
     * @author Nick
     * @return String
     */
    private String apiGetUsers()
    {
       String userstr = "USERS:";

       for(Map.Entry<InetAddress,String> entry : users.entrySet())
       {
           userstr += entry.getValue() + "|" + entry.getKey() + ":";
       }

       userstr = userstr.substring(0, userstr.length() - 1); // remove last colon

       return userstr;

    }
}
